package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func NewGroupFromFile(path string) (*Group, error) {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	group := &Group{}
	if err := json.Unmarshal(content, group); err != nil {
		return nil, err
	}

	return group, nil
}

type Group struct {
	Name       string      `json:"name"`
	Version    string      `json:"version"`
	Binaries   []string    `json:"binaries"`
	Benchmarks []Benchmark `json:"benchmarks"`
}

func (g *Group) RunBenchmarks(flags map[string]string) (tested, passed int, errs []error) {
	for _, benchmark := range g.Benchmarks {
		switch ok, err := benchmark.RunTests(flags); true {
		case err != nil:
			errs = append(errs, err)
		case ok:
			passed++
		}
	}

	return len(g.Benchmarks), passed, errs
}

type Benchmark struct {
	ID        string `json:"id"`
	Scored    bool   `json:"scored"`
	Text      string `json:"text"`
	Operation string `json:"op,omitempty"`
	Tests     []Test `json:"tests,omitempty"`
}

func (b *Benchmark) RunTests(flags map[string]string) (passed bool, err error) {
	switch b.Operation {
	case "", "all", "and":
		for _, test := range b.Tests {
			// fail benchmark if any test fails
			ok, err := test.Run(flags)
			if err != nil || !ok {
				return false, err
			}
		}
		return true, nil

	case "any", "or":
		for _, test := range b.Tests {
			// pass benchmark if any test passes
			ok, err := test.Run(flags)
			if err == nil && ok {
				return true, nil
			}
		}
		return false, nil

	default:
		return false, fmt.Errorf("unrecognized operation %q", b.Operation)
	}
}

type Test struct {
	Flag      string `json:"flag"`
	Value     string `json:"value,omitempty"`
	Operation string `json:"op,omitempty"`
	Set       bool   `json:"set,omitempty"`
}

func (t *Test) Run(flags map[string]string) (passed bool, err error) {
	flagval, ok := flags[t.Flag]
	// t.Set might not be specified, but is assumed true by the presence of t.Value
	flagExpected := t.Set || t.Value != ""

	switch {
	// flag is set in flagset but should not be
	case ok && !flagExpected:
		return false, nil
	// flag is not set in flagset and should be
	case !ok && flagExpected:
		return false, nil
	// flag is not set in flagset and should not be
	case !ok && !flagExpected:
		return true, nil
	}

	switch strings.ToLower(t.Operation) {
	// flag should be equal to test value
	case "", "eq", "is":
		return t.Value == flagval, nil
	// flag should not be equal to test value
	case "is not", "neq", "not":
		return t.Value != flagval, nil
	// flag float value should be strictly greater than test float value
	case "greater than", "gt":
		flagvalf, tvalf, err := argsToFloats(flagval, t.Value)
		if err != nil {
			return false, err
		}
		return flagvalf > tvalf, nil
	// flag float value should be strictly less than test float value
	case "less than", "lt":
		flagvalf, tvalf, err := argsToFloats(flagval, t.Value)
		if err != nil {
			return false, err
		}
		return flagvalf < tvalf, nil
	// flag float value should be greater than or equivalent to test float value
	case "at least", "gte":
		flagvalf, tvalf, err := argsToFloats(flagval, t.Value)
		if err != nil {
			return false, err
		}
		return flagvalf >= tvalf, nil
	// flag float value should be less than or equivalent to test float value
	case "at most", "lte":
		flagvalf, tvalf, err := argsToFloats(flagval, t.Value)
		if err != nil {
			return false, err
		}
		return flagvalf <= tvalf, nil
	// flag should contain test value
	case "contains", "has", "in":
		return strings.Contains(flagval, t.Value), nil
	// flag should not contain test value
	case "missing", "not has", "not in":
		return !strings.Contains(flagval, t.Value), nil
	default:
		return false, fmt.Errorf("unrecognized operation %q", t.Operation)
	}
}

// argsToFloats returns its two string operands parsed as 64-bit floats, and a non-nil error if
// they cannot be parsed.
func argsToFloats(s1, s2 string) (f1, f2 float64, err error) {
	f1, err = strconv.ParseFloat(s1, 64)
	if err != nil {
		return 0, 0, err
	}
	f2, err = strconv.ParseFloat(s2, 64)
	if err != nil {
		return 0, 0, err
	}

	return f1, f2, nil
}
