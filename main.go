package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"text/tabwriter"
)

const (
	outputHeaders = "ID\tSCORED\tTEXT\tPASSED"
	passedGlyph   = "✓"
	failedGlyph   = "☓"
	usage         = `Seaworthy is a utility for validating running processes against benchmark specs.
Usage: %s [flags] [file ...]
Flags:
`
)

var (
	pid       uint
	quiet     bool
	verbosity uint
)

func init() {
	flag.UintVar(&pid, "pid", 0, "ID of running process to benchmark, setting this will disable autodiscovery.")
	flag.BoolVar(&quiet, "quiet", false, "Disable all test output and logging.")
	flag.UintVar(&verbosity, "v", 0, "Increase the verbosity of logs to stderr. (0: error, 1: info, 2+: debug)")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage, os.Args[0])
		flag.PrintDefaults()
	}

	log.SetFlags(log.Ltime | log.Lshortfile)
}

func main() {
	flag.Parse()

	if flag.NArg() == 0 {
		flag.Usage()
		os.Exit(1)
	}

	var out io.Writer

	if quiet {
		log.SetOutput(ioutil.Discard)
		out = ioutil.Discard
	} else {
		out = os.Stdout
	}

	groups, err := readFiles(flag.Args()...)
	if err != nil {
		log.Fatalln(err)
	}

	var (
		process  *Process
		exitFail bool
	)

	for _, group := range groups {
		// no PID given from the command line, try to autodiscover by searching through
		// /proc/*/exe until we find something that matches one of the binaries given
		// in the benchmark spec
		if pid == 0 {
			logInfo("attempting to autodiscover process, searching for executables:", group.Binaries)
			process, err = findProcess(group.Binaries...)
			if err != nil {
				log.Fatalln(err)
			}
			logInfo("process found, using PID", process.PID)
		} else {
			process = NewProcess(pid)
			logInfo("using PID", process.PID)
		}

		cmdline, err := process.Cmdline()
		if err != nil {
			log.Fatal(err)
		}
		logDebug("process running with cmdline:", cmdline)

		args, flags := parseCmdline(cmdline)
		logDebug("args:", args)
		logDebug("flags:", flags)

		writer := tabwriter.NewWriter(out, 0, 8, 0, '\t', 0)
		fmt.Fprintln(writer, outputHeaders)

		for _, benchmark := range group.Benchmarks {
			fmt.Fprintf(writer, "%s\t%t\t%s\t", benchmark.ID, benchmark.Scored, benchmark.Text)

			switch passed, err := benchmark.RunTests(flags); true {
			case err != nil:
				fmt.Fprintf(writer, "%s (error: %s)\n", failedGlyph, err)
			case passed:
				fmt.Fprintln(writer, passedGlyph)
			case !passed:
				fmt.Fprintln(writer, failedGlyph)
				exitFail = true
			}
		}

		writer.Flush()
	}

	if exitFail {
		os.Exit(1)
	}
}

func logInfo(v ...interface{}) {
	if verbosity >= 1 {
		log.Println(v...)
	}
}

func logDebug(v ...interface{}) {
	if verbosity >= 2 {
		log.Println(v...)
	}
}
