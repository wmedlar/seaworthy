package main

import (
	"errors"
	"fmt"
	"log"
	"path/filepath"
	"strings"
)

func readFiles(filenames ...string) (groups []*Group, err error) {
	for _, filename := range filenames {
		logDebug("reading file", filename)
		group, err := NewGroupFromFile(filename)
		if err != nil {
			return nil, err
		}
		groups = append(groups, group)
	}

	return groups, nil
}

func findProcess(binaries ...string) (*Process, error) {
	var candidates []*Process

	paths, _ := filepath.Glob("/proc/[0-9]*")
	for _, path := range paths {
		p, err := NewProcessFromPath(path)
		if err != nil {
			log.Println(err)
			continue
		}

		executable, err := p.Exe()
		if err != nil {
			// executable may no longer exist
			log.Println(err)
			continue
		}

		for _, bin := range binaries {
			if strings.HasSuffix(executable, bin) {
				candidates = append(candidates, p)
				break
			}
		}
	}

	switch n := len(candidates); n {
	case 0:
		return nil, errors.New("no candidates for process, please specify PID")
	case 1:
		return candidates[0], nil
	default:
		return nil, fmt.Errorf("%d candidates for process, please specify PID: %v", n, candidates)
	}
}

func parseCmdline(args []string) (commands []string, flags map[string]string) {
	flags = make(map[string]string)

	for i := 0; i < len(args); i++ {
		arg := args[i]

		// a single or double-dash flag, potentially with a value
		if strings.HasPrefix(arg, "-") {
			var key, value string

			switch next, ok := lookahead(args, i+1); true {
			// flag has a value attached to it (e.g., --flag=value)
			case strings.Contains(arg, "="):
				pair := strings.SplitN(arg, "=", 2)
				key, value = pair[0], pair[1]
			// flag is followed by a value (e.g., --flag value)
			case ok && !strings.HasPrefix(next, "-"):
				key, value = arg, next
				// consume the next value
				i++
			// flag has no value attached to it (e.g., --flag)
			default:
				key, value = arg, "true"
			}
			// remove leading -/-- of flag
			key = strings.TrimLeft(key, "-")
			flags[key] = value
		} else {
			commands = append(commands, arg)
		}
	}

	return commands, flags
}

func lookahead(array []string, index int) (value string, ok bool) {
	if index < len(array) {
		return array[index], true
	}
	return "", false
}
