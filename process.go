package main

import (
	"io/ioutil"
	"path/filepath"
	"strconv"
	"strings"
)

// NewProcess creates a new Process from a PID and returns a pointer to it.
func NewProcess(pid uint) *Process {
	return &Process{
		PID: pid,
	}
}

// NewProcessFromPath creates a new Process from its directory in procfs,
// and returns a pointer to it.
func NewProcessFromPath(dirpath string) (*Process, error) {
	dirname := filepath.Base(dirpath)
	pid, err := strconv.ParseUint(dirname, 10, strconv.IntSize)
	if err != nil {
		return nil, err
	}

	return NewProcess(uint(pid)), nil
}

// Process is a representation of a running process and its space in the procfs.
type Process struct {
	// PID is the process' ID.
	// The range of PIDs is system-dependent, but it's safe to say they
	// will always be non-negative. fork can return -1, but this is
	// significant of an error and not an actual process or directory
	// in procfs.
	PID uint
}

// pathTo builds an absolute path to a file in this process' filesystem.
// This file may or may not exist, that determination is left to the caller.
func (p *Process) pathTo(filename string) string {
	return filepath.Join("/", "proc", strconv.FormatUint(uint64(p.PID), 10), filename)
}

// Exe returns the path to the executable of this process.
func (p *Process) Exe() (exePath string, err error) {
	link := p.pathTo("exe")
	return filepath.EvalSymlinks(link)
}

// Cmdline returns the executable and arguments used to start this process.
func (p *Process) Cmdline() (args []string, err error) {
	path := p.pathTo("cmdline")
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	split := strings.Split(string(content), "\u0000")
	splitlen := len(split)
	// trim trailing empty string
	// this is leftover from the cmdline file being terminated with a NUL
	if splitlen > 0 {
		split = split[:splitlen-1]
	}
	return split, nil
}
